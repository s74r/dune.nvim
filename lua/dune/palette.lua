local M = {}

M.base = {
  black = {
    m1   = '#0f0f0f',
    base = '#111111',
    p1   = '#151515',
    p2   = '#171717',
    p3   = '#191919',
    p4   = '#222222',
    p5   = '#2c2c2c',
    p6   = '#323232',
  },
  gray = {
    m4 = '#515151',
    m3 = '#5f5f5f',
    m2 = '#696969',
    m1 = '#7d7d7d',
    base = '#858585',
    p1 = '#929292',
    p2 = '#9c9c9c',
    p3 = '#a4a4a4',
    p4 = '#b4b4b4',
  },
  white = {
    base = '#c2cdcd',
    p1 = '#d2d6d9',
    p2 = '#e2e8ea',
  },
  beige = {
    m4 = '#45423c',
    m3 = '#54534d',
    m2 = '#716d5e',
    m1 = '#7d796b',
    base = '#969489',
    p1 = '#a09b88',
    p2 = '#aba693',
    p3 = '#b2ac92',
  },
  tan_m3 = '#544f47',
  tan = '#958976',
  --tan_p1 = '#b2a693',
  --tan_p2 = '#deceb2',
  olive = '#7e7f69',
  red_m1 = '#8d565d',
  red = '#af6b74',
  red_p1 = '#d8858e',
  magenta_m1 = "#65435e",
  magenta = '#cda0c0',
  magenta_p1 = '#e4b2d5',
  orange = '#b6906e',
  orange_p1 = '#d69779',
  yellow = '#b9a177',
  yellow_p1 = '#e4c793',
  green_m1 = '#697649',
  green = '#7e8964',
  green_p1 = '#96a377',
  aqua = '#7f968b',
  aqua_p1 = '#96b1a3',
  aqua_gray = "#899a95",
  blue_m2 = '#49536d',
  blue_m1 = '#7587a1',
  blue = '#8a9ab2',
  blue_p1 = '#abb9dc',
  bluegray = '#89979c',
}

M.redshift = {
  black = {
    m1   = '#100e0e',
    base = '#141311',
    p1   = '#191715',
    p2   = '#1d1a18',
    p3   = '#1f1d1a',
    p4   = '#25221f',
    p5   = '#2f2b28',
    p6   = '#34302c',
  },
}

return M

-- vim: filetype=lua nowrap
