local M = {}

THEME = "dune"

local util = require(THEME .. '.util')
local highlight_groups = require(THEME .. '.groups')
local defs = require(THEME .. '.definitions')

local palette = require(THEME .. '.palette')
local colors = palette.base

function M.colorscheme()
  util.reload(THEME)
  util.initialize(THEME)

  for _, hl_group in ipairs(highlight_groups) do
    local groups = hl_group.get_hl(defs, colors)

    for group, style in pairs(groups) do
      util.set_hl(group, style)
    end
  end
end

return M
