local M = {}

local palette = require(THEME .. '.palette')
local c = palette.base

local redshift = 1

if redshift == 1 then
  c = vim.tbl_deep_extend('force', c, palette.redshift or {})
end

M.syntax = {
  comment    = c.gray.m3,

  constant   = c.gray.p3,
  string     = c.bluegray,
  number     = c.orange,

  identifier = c.beige.base,
  fn         = c.beige.base,

  statement  = c.white.p2,
  operator   = c.beige.base,

  preproc    = c.beige.p2,

  types      = c.gray.p1,
  structure  = c.gray.base,

  special    = c.beige.p1,
  delimiter  = c.gray.base,

  underlined = { underline = true },
  ignore     = c.tan_m3,
  errors     = c.red,
  todo       = c.beige.p3,
}

M.ui = {
  gutter = c.black.m1,

  normal_fg = c.white.base,
  normal_bg = 'none',
  normal_nc_bg = c.black.p1,

  fold_column_fg = c.beige.m3,
  sign_column_fg = c.beige.m5,

  line_nr = c.beige.m4,
  cursor_line_nr = c.beige.p2,

  color_column = c.black.p2,
  cursor_line = c.black.p3,

  folded_fg = c.gray.m4,
  folded_bg = c.black.p1,

  statusline_fg = c.beige.p1,
  statusline_bg = c.black.p4,
  statuslinenc_fg = c.beige.m3,
  statuslinenc_bg = c.black.p4,

  tab_bar_bg = c.black.p4,
  tab_nc_fg = c.beige.m3,
  tab_nc_bg = c.black.p2,
  tab_sel_fg = c.beige.p2,
  tab_sel_bg = c.black.p1,

  visual_bg = c.black.p6,

  win_separator = c.black.p4,
  normal_float_bg = c.black.p2,

  pmenu_fg = c.beige.base,
  pmenu_bg = c.black.p4,
  pmenu_sel_fg = c.black.base,
  pmenu_sel_bg = c.beige.base,
  pmenu_sbar_bg = c.black.p6,
  pmenu_thumb_bg = c.beige.m2,

  search_fg = c.white.p2,
  search_bg = c.magenta_m1,
  cur_search_fg = c.black.base,
  cur_search_bg = c.magenta_p1,
  substitute = c.yellow_p1,

  match_paren_fg = c.white.p2,
  match_paren_bg = c.blue_m2,

  wild_menu_fg = c.orange,

  title = c.white.p1,
  folder = c.aqua,
  float_border = c.beige.base,
  float_title = c.white.p2,

  non_text = c.beige.m4,
  conceal = c.beige.m4,

  spell_bad = c.red,
  spell_cap = c.orange,

  error_msg = c.red,
  warning_msg = c.orange,
  more_msg = c.beige.p1,
  mode_msg = c.beige.p1,
  question = c.beige.p1,

  diff_add = c.green,
  diff_change = c.blue,
  diff_delete = c.red,
  diff_text = c.beige.p1,

  cursor_fg = c.black.base,
}


-- file tupes

M.diff_added = c.green_m1
M.diff_changed = c.blue_m1
M.diff_removed = c.red_m1
M.diff_file = c.orange
M.diff_line = c.magenta
M.diff_index_line = c.orange
M.diff_new_file = c.green_m1
M.diff_old_file = c.red_m1

M.md_link_text = c.aqua

M.sh_command_sub = c.beige.base
M.sh_cmd_sub_region = c.beige.p2
M.sh_deref = c.tan
M.sh_deref_var = c.beige.p2
M.sh_deref_simple = c.beige.p2
M.sh_no_quote = c.beige.p1

-- diagnostic & lsp
M.lsp_ref_read = c.blue
M.lsp_code_lens_fg = c.beige.m2
M.lsp_code_lens_bg = c.black.p5
M.diag_deprecated = c.yellow
M.diag_unnecessary = c.yellow
M.diag_error = c.red
M.diag_ok = c.green_p1
M.diag_hint = c.blue
M.diag_info = c.beige.p1
M.diag_warn = c.orange
M.diag_virtual = c.black.p5

M.plugins = {
  ibl_indent = c.black.p4,
  ibl_scope = c.black.p6,

  tree_normal = c.black.p2,
  tree_normal_nc = c.black.p2,
  tree_root = c.aqua,
  tree_win_sep = c.beige.m2,

  git_signs_add = c.green_m1,
  git_signs_change = c.blue_m1,
  git_signs_delete = c.red_m1,
}

-- terminal colors
M.tc = {
  color0 = c.black.base,
  color1 = c.red,
  color2 = c.green,
  color3 = c.yellow,
  color4 = c.blue,
  color5 = c.magenta,
  color6 = c.aqua,
  color7 = c.white.base,
  color8 = c.beige.m5,
  color9 = c.red_p1,
  color10 = c.green_p1,
  color11 = c.yellow_p1,
  color12 = c.blue_p1,
  color13 = c.magenta_p1,
  color14 = c.aqua_p1,
  color15 = c.white.p1,
}

return M

-- vim: filetype=lua nowrap
