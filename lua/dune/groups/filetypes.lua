local M = {}

M.get_hl = function(def, colors)
  return {
    diffAdded = { fg=def.diff_added },
    diffChanged = { fg=def.diff_changed },
    diffRemoved = { fg=def.diff_removed },
    diffFile = { fg=def.diff_file },
    diffLine = { fg=def.diff_line },
    diffIndexLine = { fg=def.diff_index_line },
    diffNewFile = { fg=def.diff_new_file, italic=true },
    diffOldFile = { fg=def.diff_old_file, italic=true },

    markdownLinkText = { fg=def.md_link_text },
    markdownUrl = { link="Underlined" },

    tmuxHiddenVar = { link="specialComment" },

    -- shCaseLabel = cleared
    -- shExpr = cleared
    --shShellVariables = { fg=def.sh_shell_variables, bold=true },
    shCommandSub = { fg = def.sh_command_sub, italic = true },
    shCmdSubRegion = { fg = def.sh_cmd_sub_region },
    shDeref = { fg = def.sh_deref, bold = true },
    shDerefVar = { fg = def.sh_deref_var },
    --shDerefVarArray = { link="Special" },
    --shDerefSimple = { fg=def.sh_deref_simple },
    --shOperator = { link="Operator" },
    --shRange = { link="shOperator" },
    --shSnglCase = { link="Statement" },
    --shSetList = { link="Identifier" },
    --shVariable = { link="shSetList" },
    shLoop = { link="Repeat" },
    shNoQuote = { fg=def.sh_no_quote, bold=true },

    --zshConditional    = { link="Conditional" },
    --zshCase           = { link="zshConditional" },
    --zshCaseIn         = { link="zshCase" },
    --zshCommands       = {  },
    --zshComment        = { link="Comment" },
    --zshDelimiter      = {  },
    --zshDeref          = {  },
    --zshDereferencing  = {  },
    --zshLongDeref      = {  },
    --zshDollarVar      = {  },
    --zshException      = { link="Exception" },
    --zshFunction       = { link="Function" },
    --zshGlob           = {  },
    --zshHereDoc        = {  },
    --zshKeyword        = { link="Keyword" },
    --zshVariable       = { link="Identifier" },
    --zshVariableDef    = {  },
  }
end

return M
