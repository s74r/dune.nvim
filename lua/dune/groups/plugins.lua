local M = {}

function M.get_hl(def, colors)
  local plugins = def.plugins

  return {
    IblIndent = { fg = plugins.ibl_indent },
    IblScope = { fg = plugins.ibl_scope },

    GitSignsAdd = { fg = plugins.git_signs_add, bg = def.ui.gutter },
    GitSignsChange = { fg = plugins.git_signs_change, bg = def.ui.gutter },
    GitSignsDelete = { fg = plugins.git_signs_delete, bg = def.ui.gutter },

    NvimTreeNormal = { bg = plugins.tree_normal },
    NvimTreeNormalNC = { bg = plugins.tree_normal_nc },
    NvimTreeRootFolder = { fg = plugins.tree_root, bold = true },
    NvimTreeWinSeparator = { fg = plugins.tree_win_sep },
  }
end

return M

-- vim:filetype=lua nowrap
