local M = {}

function M.get_hl(def, colors)
  local ui = def.ui
  local c = colors

  return {
    Normal       = { fg = ui.normal_fg, bg = ui.normal_bg },
    NormalNC     = { fg = ui.normal_fg, bg = ui.normal_nc_bg },

    FoldColumn   = { fg = ui.fold_column_fg, bg = ui.gutter },
    SignColumn   = { fg = ui.sign_column_fg, bg = ui.gutter },

    LineNr       = { fg = ui.line_nr },
    CursorLineNr = { fg = ui.cursor_line_nr, bold = true },

    ColorColumn  = { bg = ui.color_column },
    CursorLine   = { bg = ui.cursor_line },
    CursorColumn = { link = "CursorLine" },

    Folded       = { fg = ui.folded_fg, bg = ui.folded_bg, italic = true },

    StatusLine   = { fg = ui.statusline_fg, bg = ui.statusline_bg },
    StatusLineNC = { fg = ui.statuslinenc_fg, bg = ui.statuslinenc_bg },

    TabLineFill  = { bg = ui.tab_bar_bg },
    TabLineSel   = { fg = ui.tab_sel_fg, bg = ui.tab_sel_bg, bold = true },
    TabLine      = { fg = ui.tab_nc_fg, bg = ui.tab_nc_bg },

    Pmenu        = { fg = ui.pmenu_fg, bg = ui.pmenu_bg },
    PmenuSel     = { fg = ui.pmenu_sel_fg, bg = ui.pmenu_sel_bg, bold = true },
    PmenuSbar    = { bg = ui.pmenu_sbar_bg },
    PmenuThumb   = { bg = ui.pmenu_thumb_bg },

    WildMenu     = { fg = ui.wild_menu_fg },

    Title        = { fg = ui.title, bold = true },

    Directory    = { fg = ui.folder, bold = true },

    Visual       = { bg = ui.visual_bg },

    WinSeparator = { fg = ui.win_separator, bg = ui.win_separator },

    NormalFloat  = { bg = ui.normal_float_bg },
    FloatBorder  = { fg = ui.float_border },
    FloatTitle   = { fg = ui.float_title },

    Search       = { fg = ui.search_fg, bg = ui.search_bg, bold = true },
    CurSearch    = { fg = ui.cur_search_fg, bg = ui.cur_search_bg, bold = true },
    IncSearch    = { link = "CurSearch" },
    Substitute   = { fg = ui.substitute, underline = true },
    MatchParen   = { fg = ui.match_paren_fg, bg = ui.match_paren_bg, bold = true },

    NonText      = { fg = ui.non_text, },
    EndOfBuffer  = { link = "NonText" },
    Conceal      = { fg = ui.conceal, italic = true },
    SpecialKey   = { link = "Conceal" },

    SpellBad     = { fg = ui.spell_bad, undercurl = true },
    SpellCap     = { fg = ui.spell_cap, undercurl = true },
    SpellLocal   = { link = "SpellCap" },
    SpellRare    = { link = "SpellCap" },

    ErrorMsg     = { fg = ui.error_msg },
    WarningMsg   = { fg = ui.warning_msg },
    MoreMsg      = { fg = ui.more_msg },
    ModeMsg      = { fg = ui.mode_msg },
    Question     = { fg = ui.question },

    DiffAdd      = { fg = ui.diff_add },
    DiffChange   = { fg = ui.diff_change },
    DiffDelete   = { fg = ui.diff_delete },
    DiffText     = { fg = ui.diff_text },

    Cursor       = { fg = ui.cursor_fg, bg = c.white.base },
    TermCursor   = { link = "Cursor" },
    TermCursorNC = { link = "Cursor" },
    lCursor      = { link = "Cursor" },
    CursorIM     = { link = "Cursor" },

  }
end

return M

-- vim: filetype=lua nowrap
