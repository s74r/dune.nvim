return {
  require(THEME .. ".groups.theme"),
  require(THEME .. ".groups.syntax"),
  require(THEME .. ".groups.filetypes"),
  require(THEME .. ".groups.lsp"),
  require(THEME .. ".groups.plugins"),
}
