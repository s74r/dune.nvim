local M = {}

function M.get_hl(def, colors)
  local S = def.syntax

  return {
    Comment     = (S.comment and { fg = S.comment, italic = true }) or { italic = true },

    Constant    = (S.constant and { fg = S.constant, italic = true }) or { italic = true },
    String      = (S.string and { fg = S.string }) or { link = "Constant" },
    Character   = (S.character and { fg = S.character, italic = true }) or { link = "Constant" },
    Number      = (S.number and { fg = S.number, italic = true }) or { link = "Constant" },
    Boolean     = (S.boolean and { fg = S.boolean, italic = true }) or { link = "Constant" },
    Float       = (S.float and { fg = S.float, italic = true }) or { link = "Number" },

    Identifier  = (S.identifier and { fg = S.identifier, bold = true }) or { bold = true },
    Function    = (S.fn and { fg = S.fn, italic = true }) or { italic = true },

    Statement   = (S.statement and { fg = S.statement, bold = true }) or { bold = true },
    Conditional = (S.conditional and { fg = S.conditional, bold = true }) or { link = "Statement" },
    Repeat      = (S.repeats and { fg = S.repeats, bold = true }) or { link = "Statement" },
    Label       = (S.label and { fg = S.label, bold = true }) or { link = "Statement" },
    Operator    = (S.operator and { fg = S.operator, bold = true }) or { link = "Statement" },
    Keyword     = (S.keyword and { fg = S.keyword, bold = true }) or { link = "Statement" },
    Exception   = (S.exception and { fg = S.exception, bold = true }) or { link = "Statement" },

    PreProc     = (S.preproc and { fg = S.preproc, bold = true }) or { bold = true },
    Include     = (S.include and { fg = S.include, bold = true }) or { link = "PreProc" },
    Define      = (S.define and { fg = S.define, bold = true }) or { link = "PreProc" },
    Macro       = (S.macro and { fg = S.macro, bold = true }) or { link = "PreProc" },
    PreCondit   = (S.pre_condit and { fg = S.pre_condit, bold = true }) or { link = "PreProc" },

    Type          = (S.types and { fg = S.types }) or { link = "Normal" },
    StorageClass  = (S.storage_class and { fg = S.storage_class }) or { link = "Type" },
    Structure     = (S.structure and { fg = S.structure }) or { link = "Type" },
    Typedef       = (S.typedef and { fg = S.typedef }) or { link = "Type" },

    Special         = (S.special and { fg = S.special, italic = true}) or { italic = true },
    SpecialChar     = (S.sp_char and { fg = S.sp_char, italic = true }) or { link = "Special" },
    Tag             = (S.tag and { fg = s.tag, italic = true }) or { link = "Special" },
    Delimiter       = (S.delimiter and { fg = S.delimiter, bold = true }) or { link = "Special" },
    SpecialComment  = (S.sp_comment and { fg = S.sp_comment, italic = true }) or { link = "Special" },
    Debug           = (S.debug and { fg = S.debug, italic = true }) or { link = "Special" },

    Underlined  = { bold = true, underline = true },
    Ignore      = (S.ignore and { fg = S.ignore }) or { link = "Comment" },
    Error       = (S.errors and { fg = S.errors }) or { fg = colors.red },
    Todo        = (S.todo and { fg = S.todo, bold = true, underline = true }) or { link = "Underlined" },
  }
end

return M

-- vim: filetype=lua nowrap

