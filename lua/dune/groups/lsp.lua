local M = {}

function M.get_hl(def, colors)
  return {
    DiagnosticDeprecated = { fg = def.diag_deprecated, italic = true },
    DiagnosticUnnecessary = { fg = def.diag_unnecessary, italic = true },
    DiagnosticError = { fg = def.diag_error },
    DiagnosticHint = { fg = def.diag_hint },
    DiagnosticInfo = { fg = def.diag_info },
    DiagnosticOk = { fg = def.diag_ok },
    DiagnosticWarn = { fg = def.diag_warn },
    DiagnosticSignError = { link = "DiagnosticError" },
    DiagnosticSignHint = { link = "DiagnosticHint" },
    DiagnosticSignInfo = { link = "DiagnosticInfo" },
    DiagnosticSignOk = { link = "DiagnosticOk" },
    DiagnosticSignWarn = { link = "DiagnosticWarn" },
    DiagnosticUnderlineError = { fg = def.diag_error, underline = true },
    DiagnosticUnderlineHint = { fg = def.diag_hint, underline = true },
    DiagnosticUnderlineInfo = { fg = def.diag_info, underline = true },
    DiagnosticUnderlineOk = { fg = def.diag_ok, underline = true },
    DiagnosticUnderlineWarn = { fg = def.diag_warn, underline = true },
    DiagnosticVirtualTextError = { fg = def.diag_error, bg = def.diag_virtual },
    DiagnosticVirtualTextHint = { fg = def.diag_hint, bg = def.diag_virtual },
    DiagnosticVirtualTextInfo = { fg = def.diag_info, bg = def.diag_virtual },
    DiagnosticVirtualTexttOk = { fg = def.diag_ok, bg = def.diag_virtual },
    DiagnosticVirtualTextWarn = { fg = def.diag_warn, bg = def.diag_virtual },

    LspReferenceRead = { fg = def.lsp_ref_read },
    LspReferenceText = { link = "LspReferenceRead" },
    LspReferenceWrite = { link = "LspReferenceRead" },
    LspCodeLens = { fg = def.lsp_code_lens_fg, bg = def.lsp_code_lens_bg },

    NotifyERRORIcon = { link = "DiagnosticError" },
    NotifyERRORTitle = { link = "DiagnosticError" },
    NotifyDEBUGIcon = { link = "DiagnosticHint" },
    NotifyDEBUGTitle = { link = "DiagnosticHint" },
    NotifyTRACEIcon = { link = "DiagnosticHint" },
    NotifyTRACETitle = { link = "DiagnosticHint" },
    NotifyINFOIcon = { link = "DiagnosticInfo" },
    NotifyINFOTitle = { link = "DiagnosticInfo" },
    NotifyWARNIcon = { link = "DiagnosticWarn" },
    NotifyWARNTitle = { link = "DiagnosticWarn" },
  }

end

return M
