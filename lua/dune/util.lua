local M = {}

function M.initialize()
  vim.api.nvim_command("hi clear")
  if vim.fn.exists("syntax on") then
    vim.api.nvim_command("syntax reset")
  end
  vim.o.termguicolors = true
  vim.g.colors_name = THEME
end

function M.set_hl(group, style)
  vim.api.nvim_set_hl(0, group, style)
end

function M.reload(THEME)
  for name, _ in pairs(package.loaded) do
    if name:match('^' .. THEME) then
      package.loaded[name] = nil
    end
  end
end

return M
