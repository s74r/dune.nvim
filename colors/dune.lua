THEME = 'dune'

require(THEME .. '.util').reload(THEME)
require(THEME).colorscheme(THEME)
